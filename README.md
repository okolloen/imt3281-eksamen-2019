# IMT3281-eksamen-2019

Oppgavesettet er todelt, der første del er en GUI applikasjon og den andre delen er en nettverksserver for en tenkt (ikke eksisterende) protokoll for meldingsutveksling.

## Oppgave 1 (teller 60%)

### a)

Skriv koden som skal til for å tilfredsstille testene i no.ntnu.imt3281.eksamen.exchange_rates.ExchangeRatesTest.

Klassen(e) du skal skrive benyttes for å hente og parse data om valutakurser fra [Norges bank](https://www.norges-bank.no/tema/Statistikk/Valutakurser/?tab=api) (data som benyttes er daglige kurser, alle valuter). En kan her selv bestemme hvilket datointerval en er interessert i å benytte. Med unntak av den siste testen benyttes en dump med data fra 27 september til 4 oktober 2019 i testene.

De ulike testene er beskrevet i ExchangeRatesTest, klassen ExchangeRates teller 50 % av totalen på denne oppgaven.

Dersom du må gi opp på oppgave a, døp om klassen ExchangeRates til ExchangeRatesAttempt (legg også til "Attempt" til slutt i klassenavnet på andre klasser du eventuelt har skrevet) og gjør en commit med meldingen "Gir opp oppgave 1a" og push dette til bitbucket. Send så en e-post med URL til repositoret til oeivind.kolloen@ntnu.no så vil du få løsningsforslaget for oppgave 1a i retur. Oppgave 1a vil da bli bedømt ut ifra den koden du har når du gjør denne commiten.

### b)
 
Lag en enkel JavaFX applikasjon som lar brukeren skrive inn et beløp i norske kroner og velge valuta det skal konverteres til fra en drop down boks. I en annen tekstboks skal det så vises hva dette blir i valgt valuta. Dette kan se ut som vist under.

![Skjermdump](https://bitbucket.org/okolloen/imt3281-eksamen-2019/downloads/curencyConverter.gif)

Legg definisjonen av grensesnittet i en FXML fil som lastes inn og vises av applikasjonen, legg så koden for funksjonaliteten i kontrolleren til denne FXML   filen.

Verdien i den valgte valutaen skal oppdateres fortløpende når det skrives i feltet for norske kroner og når en ny valuta velges.

Denne deloppgaven teller 20% av totalen på denne oppgaven.

### c)

Lag en ny FXML fil med tilhørende kontroller klasse, denne skal vise en graf med oversikt over valgt valutakurs for de siste 30 dager slik som vist under. Bruk en vbox i applikasjonen fra oppgave b) slik at du får vist både innoldet du lastet inn i b) og innholdet fra den FXML filen du skal lage i denne oppgaven. 
 
![Skjermdump](https://bitbucket.org/okolloen/imt3281-eksamen-2019/downloads/valutakurser.gif)

Bruk en javafx.scene.chart.LineChart komponent for å vise grafen. Denne deloppgaven teller 30% av totalen for denne oppgaven.


## Oppgave 2 (teller 40%)

[I wikien](https://bitbucket.org/okolloen/imt3281-eksamen-2019/wiki/MicroMessagingProtocol) finnes en beskrivelse av en tenkt kommunikasjonsprotokoll (MicroMessagingProtocol, MMP) som du nå skal skrive en server for.

Det finnes noe kode for denne serveren i no.ntnu.imt3281.eksamen.mmp.server.MMPServer, bruk denne koden som utgangspunkt for å skrive koden som skal til for å få testene i no.ntnu.imt3281.eksamen.mmp.server.MMPServerTest til å passere.

Legg til de egenskaper/variable og funksjoner du mener trengs i denne klassen etter hvert for å få testene til å passere. For oppgave a-e så kan du også her be om løsningforslag dersom du ikke klarer en av oppgavene. Døp om klassen til MMPServer_Attempt_(hvilken oppgave det gjelder (A-E)) commit, push og send en e-post til oeivind.kolloen@ntnu.no og be om løsningsforslag for aktuell oppgave. Du vil da få løsningsforslaget komplett til og med den oppgaven du ber om, denne og de tidligere oppgavene vil bli bedømt ut ifra hva du har gjort så langt.

### a)
Skriv først en metode som starter en ny tråd med en ServerSocket som lytter på port 4567 og når en forbindelse blir gjort til denne porten oppretter et nytt MMPServer.Client objekt og kaller MMPServer.addClient med dette client objektet. NB, koden for MMPServer.addClient skal skrives i oppgave 2b. Denne delen teller 10% av totalen for oppgave 2. Det er ingen tester for denne funksjonaliteten. 

### b)
Skriv koden som skal til for å få testen MMPServerTest.addClient til å passere. Dette er innholdet i MMPServer.addClient metoden. Teller 15% av totalen for oppgave 2.

### c)
Skriv koden (utvid MMPServer.addClient) som skal til for å få testen MMPServerTest.addClientWithUnamePwd til å passere. NB, for denne oppgaven kan du hardkode brukernavn/passord inn i serveren. Legg merke til at brukernavn/passord kun skal kreves dersom en oppretter MMPServer objektet med *true*  som parameter til konstruktoren til MMPServer. Teller 5% av totalen for oppgave 2.

### d)
Skriv koden som skal til for å få testen MMPServerTest.disconnect til å passere. Du trenger nå en liste over alle klienter og en tråd som står å sjekker alle klientene og om de har sendt en melding til serveren. Bruk kun EN tråd for å lese fra alle klientene, dersom en client har sendt en disconnect melding skal svaret til klienten sendes fra samme tråden. Teller 25% av totalen for oppgave 2.

### e) 
Skriv koden som skal til for å få testen MMPServerTest.subscribe til å passere. Du trenger nå å ta vare på hvilke klienter som skal ha meldinger fra hvilke kanaler. Svaret skal fortsatt sendes fra den samme tråden som lytter på meldinger. Teller 15% av totalen for oppgave 2.

### f)
Skriv koden som skal til for å få testen MMPServerTest.publish til å passere. Du må nå skrive koden som skal til for å sende meldinger til alle klienter som skal ha meldinger fra en gitt kanal. Send også disse meldingene direkte i samme tråden som lytter på meldinger. Dvs at så snart en meldinger en mottatt så skal den sendes ut til alle klienter som abonnerer på den kanalen. Teller 20% av totalen for oppgaven. Om all kode så langt er skrevet riktig så skal nå også testen MMPServer.multiClient passere. 

### g) 
Det at koden håndterer at klienten sender disconnect eller bare kobles fra nettet (en ikke lenger kan sende data til klientene) og at disse klientene fjernes fra listen over alle klienter og fra alle kanaler de abonnerer på utgjør de siste 10% av totalen for oppgaven.

*NB, for å få full uttelling på oppgaven forutsettes det at streams og disses mulighet for parallellitet benyttes på en god måte.*

